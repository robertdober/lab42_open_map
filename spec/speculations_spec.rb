require "speculate_about"

RSpec.describe "Speculations" do
  shared_examples_for "a speculation" do |speculation|
    speculate_about speculation, alternate_syntax: true
  end
  context "README.md" do
    it_behaves_like "a speculation", description
  end
end
