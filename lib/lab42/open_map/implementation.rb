Warning[:experimental] = false

module Lab42
  class OpenMap
    module Implementation
      def []=(key, value)
        raise ArgumentError, "#{key.inspect} is not a symbol" unless Symbol === key
        @hash[key] = value
      end

      def deconstruct_keys(keys)
        @hash.slice(*keys)
      end

      def merge(**kwds)
        _check_symbolic_keys!(kwds)
        self.class.new(**@hash.merge(kwds))
      end

      def sans(*keys)
        self.class.new(**without(*keys))
      end

      def update(**kwds)
        _check_symbolic_keys!(kwds)
        @hash.update(kwds)
      end

      def to_h
        @hash.clone
      end

      def without(*keys)
        ( self.keys - keys.flatten )
          .inject( {} ) do |r, k|
            r.update(k => self[k])
          end
      end

      private

      def _check_symbolic_keys!(kwds)
        invalid_kwds = kwds.keys.reject{Symbol === _1}
        raise ArgumentError, "the following keys are not symbols: #{invalid_kwds.inspect}" unless
          invalid_kwds.empty?
      end

      def method_missing(name, *args)
        assignment = name[-1] == "="
        name = name[0..-2].to_sym if assignment
        super unless has_key?(name)
        if assignment
          raise ArgumentError, "assignment needs a value" if args.empty?
          self[name] = args.first
        else
          self[name]
        end
      end
    end
  end
end
