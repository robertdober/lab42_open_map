require "lab42/forwarder3/include"
require_relative "./open_map/implementation"
module Lab42
  class OpenMap
    extend Forwarder
    include Enumerable
    forward_all :[], :each, :each_pair, :empty?, :fetch, :has_key?, :inject, :keys, :size, :slice, :values, to: :@hash

    include Implementation

    private

    def initialize(**kwds)
      _check_symbolic_keys!(kwds)
      @hash = kwds 
    end
  end
end

