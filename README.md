[![Gem Version](https://badge.fury.io/rb/lab42_open_map.svg)](http://badge.fury.io/rb/lab42_open_map)

# Lab42::OpenMap

OpenMap = OpenStruct with a rich Map API

**N.B.** All these code examples are verified with [the speculate_about gem](https://rubygems.org/gems/speculate_about/)  

## Context Quick Starting Guide

Given an `OpenMap` 
```ruby
    require "lab42/open_map/include" # aliases Lab42::OpenMap as OpenMap
    let(:pet) { OpenMap.new }
```

Then we can see that it can be empty
```ruby
    expect( pet ).to be_empty
```

And that we can add fields like to a hash

```ruby
    pet[:name] = "Furfur"
    expect( pet[:name] ).to eq("Furfur")
```

However we are **not** a hash.

Example: Only Symbol Keys please

```ruby
    expect{ pet["name"] = nil }.to raise_error(ArgumentError, %{"name" is not a symbol})
```

And that holds for construction too

```ruby
    expect{ OpenMap.new("verbose" => false) }
      .to raise_error(ArgumentError, %{the following keys are not symbols: ["verbose"]})
```

But many `Hash` methods are applicable to `OpenMap` 

Given that

```ruby
    let (:dog) {OpenMap.new(name: "Rantanplan", breed: "You are kidding?")}
```
Then we can access it like a hash
```ruby
    expect( dog.keys ).to eq(%i[name breed])
    expect( dog.values ).to eq(["Rantanplan", "You are kidding?"])
    expect( dog.size ).to eq(2)
    expect( dog.map.to_a ).to eq([[:name, "Rantanplan"], [:breed, "You are kidding?"]])
```

And we can use slice and get a nice counterpart `without` 

```ruby
    expect( dog.slice(:name) ).to eq(name: "Rantanplan")
    expect( dog.slice(:name, :breed) ).to eq(name: "Rantanplan", breed: "You are kidding?")
    expect( dog.without(:breed) ).to eq(name: "Rantanplan")
    expect( dog.without(:name, :breed) ).to eq({})
```

Example: `each_pair`
```ruby
  expect( dog.each_pair.to_a ).to eq([[:name, "Rantanplan"],[ :breed, "You are kidding?"]])
    
```

`each_pair` is important for the following to work
Given we have `OpenStruct` 
```ruby
    require "ostruct"
```
Then we can create one from an `OpenMap` 
```ruby
  struct_dog = OpenStruct.new(dog)
  expect( struct_dog.name ).to eq("Rantanplan")
    
```

And last, but certainly not least: _Named Access_
```ruby
    expect(dog.name).to eq("Rantanplan") 
    dog.breed = "still unknown"
    expect( dog.values ).to eq(["Rantanplan", "still unknown"])
```


## Context All About Named Access

Given the same dog again
```ruby
    let (:dog) {OpenMap.new(name: "Rantanplan", breed: "You are kidding?")}
```

Then we cannot access a nonexistant field by name
```ruby
    expect{ dog.age }.to raise_error(NoMethodError, %r{\Aundefined method `age' for})
```

And we cannot create a new one either
```ruby
    expect{ dog.age = 10 }.to raise_error(NoMethodError, %r{\Aundefined method `age' for})
```

But we still can create new fields with `[]=` or update
```ruby
    dog.update( age: 10 ) 
    expect( dog.age ).to eq(10)
```
And of course the `update` method preserves our symbol keys only property
```ruby
    expect{ dog.update("verbose" => true)  } 
      .to raise_error(ArgumentError, %{the following keys are not symbols: ["verbose"]})
```

## Context Methods that create new `OpenMap` objects

Given a cat now, cannot risk losing half of the pet loving community ;)
```ruby
  let(:garfield) {OpenMap.new(name: "Garfield", yob: 1976, creator: "Jim Davis")} # Yes under a differnt name, but still
  let!(:nermal) { garfield.merge(name: "Nermal", yob: 1979)}
```
Then exactly the following can be certified
```ruby
    expect( nermal ).to be_kind_of(OpenMap)
    expect( garfield.values ).to eq(["Garfield", 1976, "Jim Davis"])
    expect( nermal.values ).to eq(["Nermal", 1979, "Jim Davis"])
```

We also have a counterpart to `without`, called `sans`
And with `sans` we get this

```ruby
    partial_garfield = garfield.sans(:yob, :creator) 
    expect( partial_garfield.to_h ).to eq(name: "Garfield")
    expect( garfield.values ).to eq(["Garfield", 1976, "Jim Davis"])
```

## Context Hash like Protocol

We have already seen that `[]`, `slice`, `size` and friends act like on hashes, let us document
the other _Hashlike_ methods here

Given a nice little `OpenMap` 
```ruby
    let(:my_map) { OpenMap.new(street: "Champs Elysée", city: "Paris", country: "France") }
```

### `fetch` 

Then we can fetch existing values
```ruby
    expect( my_map.fetch(:city) ).to eq("Paris")
```
And we have to be a little bit more careful with non existing values
```ruby
    expect( my_map.fetch(:zip, 75008) ).to eq(75008)
    expect( my_map.fetch(:number) { 42 } ).to eq(42)
    expect{ my_map.fetch(:continent) }.to raise_error(KeyError, "key not found: :continent" )
```

### Pattern Matching with `deconstruct_keys`

And we can pattern match
```ruby
    my_map in {city: city, street: street}
    expect( [street, city] ).to eq(["Champs Elysée", "Paris"])
    expect{ my_map in {city: "Bordeaux"} }.to raise_error(NoMatchingPatternError)
```

### `entries`

And with not much to say about
```ruby
    expect( my_map.entries ).to eq([[:street, "Champs Elysée"], [:city, "Paris"], [:country, "France"]])
```

# LICENSE

Copyright 2020 Robert Dober robert.dober@gmail.com

Apache-2.0 [c.f LICENSE](LICENSE)
