# v0.1.2 2020-12-01

- forward of `fetch` to `@hash` 
- implementation of `deconstruct_keys` → Pattern Matching


# v0.1.1 2020-11-22

- include `Enumerable` and only forward `each` 
- forward `each_pair` 
