$:.unshift( File.expand_path( "../lib", __FILE__ ) )
require "lab42/open_map/version"
version = Lab42::OpenMap.version
Gem::Specification.new do |s|
  s.name        = 'lab42_open_map'
  s.version     = version
  s.summary     = 'OpenMap = OpenStruct with rich Map API'
  s.description = %{OpenMap = OpenStruct with a rich API, e.g. #merge, #slice, #without,...}
  s.authors     = ["Robert Dober"]
  s.email       = 'robert.dober@gmail.com'
  s.files       = Dir.glob("lib/**/*.rb")
  s.files      += %w{LICENSE README.md}
  s.homepage    = "https://bitbucket.org/robertdober/lab42_open_map"
  s.licenses    = %w{Apache-2.0}

  s.required_ruby_version = '>= 2.7.0'
  s.add_dependency 'lab42_result', '~> 0.1'
  s.add_dependency 'lab42_forwarder3', '~> 0.1'

  s.add_development_dependency 'pry', '~> 0.10'
  s.add_development_dependency 'pry-byebug', '~> 3.9'
  s.add_development_dependency 'rspec', '~> 3.10'
  # s.add_development_dependency 'travis-lint', '~> 2.0'
end
